# from parent image
FROM archlinux/archlinux:latest

LABEL maintainer="Dan Olesen <olesendan@gmail.com>"

RUN pacman -Syu --noconfirm && yes | \
  pacman -S --noconfirm \
    bash \
    sudo \
    rsync \
    openssh && \
  pacman -Scc --noconfirm && \
  rm -rf /var/cache/pacman/pkg/*

